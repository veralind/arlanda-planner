package com.vseravno.arlandaplanner.busclient;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.vseravno.arlandaplanner.model.Flygbuss;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class BusClient {
  private final FlygbussClient flygbussClient;
  private final SwedAviaBusClient swedAviaBusClient;

  @RequiresApi(api = Build.VERSION_CODES.O)
  public BusClient() {
    flygbussClient = new FlygbussClient();
    swedAviaBusClient = new SwedAviaBusClient();
  }

  @VisibleForTesting
  public BusClient(FlygbussClient flygbussClient, SwedAviaBusClient swedAviaBusClient) {
    this.flygbussClient = flygbussClient;
    this.swedAviaBusClient = swedAviaBusClient;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  public List<Flygbuss> getBusTrips(LocalDate date, LocalTime arrivalDeadline)
      throws BusClientException {
    try {
      return swedAviaBusClient.getBusDepartures(date, arrivalDeadline);
    } catch (BusClientException e) {
      return flygbussClient.getBusDepartures(date, arrivalDeadline);
    }
  }
}
