package com.vseravno.arlandaplanner.sl.client;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.io.Resources;
import com.vseravno.arlandaplanner.model.Station;
import com.vseravno.arlandaplanner.model.trip.Trip;
import com.vseravno.arlandaplanner.model.trip.TripPart;
import com.vseravno.arlandaplanner.swedavia.SwedAviaException;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.HttpUrl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class SlClientTest {
  SlClient client;
  MockWebServer server;
  String url = "/";
  LocalTime time = LocalTime.of(11, 45);
  LocalDate date = LocalDate.of(2021, 4, 27);
  int stationId = 1500;

  private String getJson(String resourceName) {
    try {
      return Resources.toString(
          Resources.getResource(resourceName + ".json"), StandardCharsets.UTF_8);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  @BeforeEach
  void initialize() throws IOException {
    this.server = new MockWebServer();
    server.start();
    HttpUrl baseUrl = server.url(url);
    this.client = new SlClient(baseUrl.toString(), baseUrl.toString());
  }

  @AfterEach
  void tearDown() throws IOException {
    server.shutdown();
  }

  @Test
  public void slGetStationsTest() throws SlClientException {
    server.enqueue(new MockResponse().setBody(getJson("slArstaStations")));
    List<Station> stations = client.getStations("Årsta");
    Assertions.assertEquals(10, stations.size());
    Assertions.assertEquals(1541, stations.get(0).getStationId());
    Assertions.assertEquals("Årsta gård (Stockholm)", stations.get(0).getStationName());
  }

  @Test
  public void getStationsResponceExceptionTest() {
    Exception exception =
        Assertions.assertThrows(SlClientException.class, () -> client.getStations("Årsta"));
    String expectedMessage = "Failed to get Sl stations api request";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getStationsCodeExceptionTest() {
    server.enqueue(new MockResponse().setResponseCode(404));
    Exception exception =
        Assertions.assertThrows(SlClientException.class, () -> client.getStations("Årsta"));
    String expectedMessage = "Sl stations api returned code: ";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getStationsParseExceptionTest() {
    server.enqueue(new MockResponse().setBody("<xml>xml</xml>"));
    Exception exception =
        Assertions.assertThrows(SlClientException.class, () -> client.getStations("Årsta"));
    String expectedMessage = "Failed to parse Sl stations api response";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getAllDeparturesTest() throws SlClientException {
    server.enqueue(new MockResponse().setBody(getJson("arstaTrip")));

    List<Trip> allDepartures = client.getAllDepartures(time, date, stationId);
    TripPart trip = allDepartures.get(0).getTrip().get(0);
    Assertions.assertEquals(5, allDepartures.size());
    Assertions.assertEquals("buss 164", trip.getName());
    Assertions.assertEquals("Gullmarsplan", trip.getDestination());
    for (Trip allDeparture : allDepartures) {
      List<TripPart> trip1 = allDeparture.getTrip();
      for (TripPart tripPart : trip1) {
        System.out.println(tripPart.tripAsString());
      }
    }
  }

  @Test
  public void getDeparturesResponceExceptionTest() {
    Exception exception =
        Assertions.assertThrows(
            SlClientException.class, () -> client.getAllDepartures(time, date, stationId));
    String expectedMessage = "Failed to get Sl travel planner api request";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getDeparturesCodeExceptionTest() {
    server.enqueue(new MockResponse().setResponseCode(404));
    Exception exception =
        Assertions.assertThrows(
            SlClientException.class, () -> client.getAllDepartures(time, date, stationId));
    String expectedMessage = "Sl travel planner api returned code: ";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getDeparturesParseExceptionTest() {
    server.enqueue(new MockResponse().setBody("<xml>xml</xml>"));
    Exception exception =
        Assertions.assertThrows(
            SlClientException.class, () -> client.getAllDepartures(time, date, stationId));
    String expectedMessage = "Failed to parse Sl travel planner api response";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }
}
